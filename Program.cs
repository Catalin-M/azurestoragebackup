﻿using System;
using System.Threading.Tasks;
using System.Threading;

namespace AzureStorageBackup
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string originConnectionString = "";

            string destinationConnectionString = "";

            // clear both blob and table storage
            // blob delete operation can still be ongoing despite the command to delete them being
            // sent over! as such, wait 15 seconds before attempting to create/upload anything
            await BlobOperations.DeleteBlobs(destinationConnectionString);
            await TableOperations.DeleteTables(destinationConnectionString);

            TimeSpan ts = new TimeSpan(0, 0, 15);
            Console.WriteLine("Waiting 15 seconds for delete blob operation to finish."
                + " Create blob operations will fail if run right after a delete!");
            Thread.Sleep(ts);

            // download blobs async to a local folder
            await BlobOperations.DownloadBlobs(originConnectionString);

            // upload downloaded blobs async to destination account
            await BlobOperations.UploadBlobs(destinationConnectionString);

            // transfer tables from an origin account to a destination account
            await TableOperations.TransferTables(originConnectionString,
                destinationConnectionString);
            
        }
    }
}
