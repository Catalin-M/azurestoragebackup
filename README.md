### Azure Storage Backup Tool  

This is a simple console application that takes in two connection strings and transfers data from the origin to the destination.  

The application does contain **destructive commands** as it will delete all contents of the destination storage account. It starts by deleting tables and blobs in the destination storage account so that it ensures it writes to an empty account.  

It then downloads all the blobs inside the origin storage account. Each container goes into its own folder and the code assumes that's how the blobs are structured. If you just download a large number of blobs into the *downloads* folder, make sure to adjust the paths inside the `BlobOperations.cs` file. It then asynchronously uploads all the blobs to the destination account.  

Table operations are done in memory and the code will load all the entities inside a table if it's less than 100 or it will create batches of 100 entities and do batch uploads to the destination tables. It is using a generic `TableEntity` construct to hold data between transfers, but keep in mind that batch operations need to be done on the same type of entity which means that if you have `Car` entities and `Dog` entities which are different in their construction, the application will fail.  

It will output progress to the console as it is doing all these steps.  

As a **final reminder** make sure that you know what the code does. Egress charges can be incurred when downloading blobs. Depending on the storage type, there might be additional fees!!! A simple way to use the code is to ensure both storage accounts are on the same Azure Region and also that you ideally run this code off of a VM in the same region to not incur those egress charges. Azure will also charge per transfer amount so this can quikcly become VERY EXPENSIVE if you attempt transfer of large quantities of data!!!