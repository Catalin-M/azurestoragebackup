using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Azure;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;

namespace AzureStorageBackup
{
    /// <summary>
    /// Class responsible for Blob operations - download blobs from a given storage account
    /// to a download folder asynchronously, upload blobs from the download folder to a storage 
    /// account as well as remove all blob containers from a given storage account
    /// </summary>
    public static class BlobOperations
    {
        /// <summary>
        /// Method downloads all blobs from a given storage account and saves them in a folder 
        /// `downloads` in local storage. Each container will be a folder inside the `downloads`
        /// folder with each blob being downloaded into their respective folders
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static async Task DownloadBlobs(string connectionString)
        {
            // Timer that measures how long the entire operation takes
            Stopwatch timer = Stopwatch.StartNew();

            // Path to where all the blobs will be downloaded
            string downloadPath = Directory.GetCurrentDirectory() + "\\download\\";

            // Specify the StorageTransferOptions - need to read up on these :) 
            var options = new StorageTransferOptions
            {
                // Set the maximum number of workers that 
                // may be used in a parallel transfer.
                MaximumConcurrency = 8,
                // Set the maximum length of a transfer to 50MB.
                MaximumTransferSize = 50 * 1024 * 1024
            };

            // the service clients allow working at the Azure Storage level with Tables and Blobs
            BlobServiceClient originBlobServClient =
                new BlobServiceClient(connectionString);

            // To work with individual blob containers, I need to retrieve the BlobContainerItems
            Pageable<BlobContainerItem> originBlobContainers =
                originBlobServClient.GetBlobContainers();
            Console.WriteLine($"Downloading blobs from {originBlobServClient.AccountName}.");

            // Create a queue of tasks and each task downloads one file
            var tasks = new Queue<Task<Response>>();
            int downloadedBlobCount = 0;
            foreach (BlobContainerItem bci in originBlobContainers)
            {
                if ((bci.Name.Contains("-") || bci.Name.Contains("$")) == false)
                {
                    Directory.CreateDirectory(downloadPath + bci.Name);
                }

            }

            // go through all Blob containers in the origin
            foreach (BlobContainerItem bci in originBlobContainers)
            {
                if ((bci.Name.Contains("-") || bci.Name.Contains("$")) == false)
                {
                    System.Console.WriteLine("Container name: " + bci.Name);
                    BlobContainerClient originBlobContainerClient =
                    originBlobServClient.GetBlobContainerClient(bci.Name);

                    Pageable<BlobItem> originBlobsInContainer =
                        originBlobContainerClient.GetBlobs();

                    Console.WriteLine($"Downloading blobs from {bci.Name} container.");
                    foreach (BlobItem bi in originBlobsInContainer)
                    {
                        //add each BlobItem to the queue to be downloaded
                        string fileName = downloadPath + bci.Name + "\\" + bi.Name;
                        BlobClient blob = originBlobContainerClient.GetBlobClient(bi.Name);
                        tasks.Enqueue(blob.DownloadToAsync(fileName, default, options));
                        downloadedBlobCount++;
                    }
                }

            }
            // run all downloads async
            await Task.WhenAll(tasks);
            timer.Stop();
            Console.WriteLine($"Downloaded {downloadedBlobCount} in"
                + $" {timer.Elapsed.TotalSeconds} seconds");
        }

        /// <summary>
        /// Method uploads all the blobs for a given storage account from the `downloads` folder
        /// and names each container according to the name of the folder container the blob
        /// EX: \downloads\container1\1 will create a container "container1" and upload a 
        /// blob named "1"
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static async Task UploadBlobs(string connectionString)
        {
            // Timer that measures how long the entire operation takes
            Stopwatch timer = Stopwatch.StartNew();
            // Path where all the blobs were downloaded to
            string downloadPath = Directory.GetCurrentDirectory() + "\\download\\";
            BlobServiceClient destinationBlobServClient =
                new BlobServiceClient(connectionString);

            // pattern to retrieve the blob name, which in my case is just a number
            string blobPattern = @"\\[0-9]{1,}";
            // Specify the StorageTransferOptions
            BlobUploadOptions uploadOptions = new BlobUploadOptions
            {
                TransferOptions = new StorageTransferOptions
                {
                    // Set the maximum number of workers that 
                    // may be used in a parallel transfer.
                    MaximumConcurrency = 8,

                    // Set the maximum length of a transfer to 50MB.
                    MaximumTransferSize = 50 * 1024 * 1024
                }
            };
            var uploadTasks = new Queue<Task<Response<BlobContentInfo>>>();
            int uploadedBlobCounts = 0;

            string[] directories = Directory.GetDirectories(downloadPath);
            // create the blob containers to match the origin blob containers
            // based on the folder names downloaded in the download step!
            foreach (string s in directories)
            {
                DirectoryInfo di = new DirectoryInfo(s);
                destinationBlobServClient.CreateBlobContainer(di.Name);
            }

            Pageable<BlobContainerItem> destinationBlobContainers =
                destinationBlobServClient.GetBlobContainers();

            // the upload is slowed down, since I will run each folder at a time
            foreach (BlobContainerItem destBci in destinationBlobContainers)
            {
                Console.WriteLine($"Uploading blobs to {destBci.Name} container.");
                BlobContainerClient destinationBlobContainerClient =
                    destinationBlobServClient.GetBlobContainerClient(destBci.Name);
                string[] filePaths = Directory.GetFiles(downloadPath + destBci.Name);
                foreach (string s in filePaths)
                {
                    Match blobName = Regex.Match(s, blobPattern);
                    string blobString = blobName.Value.Substring(1);
                    BlobClient blob = destinationBlobContainerClient.GetBlobClient(blobString);
                    uploadTasks.Enqueue(blob.UploadAsync(s, uploadOptions));
                    uploadedBlobCounts++;
                }
            }
            // run all uploads async
            await Task.WhenAll(uploadTasks);
            timer.Stop();
            Console.WriteLine($"Uploaded {uploadedBlobCounts} files in "
                + $"{timer.Elapsed.Seconds} seconds");
        }

        /// <summary>
        /// Method to delete all blob containers in the destination - this ensures that only a clean
        /// copy of the origin is made to an empty storage account
        /// </summary>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static async Task DeleteBlobs(string connectionString)
        {
            // Timer that measures how long the entire operation takes
            Stopwatch timer = Stopwatch.StartNew();
            int blobCount = 0;
            BlobServiceClient destinationBlobServClient =
                new BlobServiceClient(connectionString);
            Pageable<BlobContainerItem> destinationBlobContainers =
                destinationBlobServClient.GetBlobContainers();
            Console.WriteLine($"Deleting blobs from {destinationBlobServClient.AccountName}");

            foreach (BlobContainerItem bci in destinationBlobContainers)
            {

                await destinationBlobServClient.DeleteBlobContainerAsync(bci.Name);
                blobCount++;
            }
            timer.Stop();
            Console.WriteLine($"Deleted {blobCount} blobs from "
                + $"{destinationBlobServClient.AccountName} in {timer.Elapsed.Seconds} seconds");
        }
    }
}