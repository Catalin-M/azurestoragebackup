using Azure;
using System;
using Azure.Data.Tables;
using Azure.Data.Tables.Models;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureStorageBackup
{
    /// <summary>
    /// Class responsible for Table Operations - transfer tables and table entities in batches 
    /// of 100 entities from an origin table to a destination table; the class can also clear a 
    /// storage account of all the tables (this is recommended before transfer to ensure no 
    /// overwrites/issues creating new tables appear)
    /// </summary>
    public static class TableOperations
    {
        /// <summary>
        /// Method transfers all tables from an origin Azure Storage account to a destination
        /// Azure Storage account. If tables exist, they are left intact and if they are missing
        /// they are created and populated in batch operations of 100 entities each
        /// </summary>
        /// <param name="originConnectionString"></param>
        /// <param name="destinationConnectionString"></param>
        public static async Task TransferTables(string originConnectionString,
            string destinationConnectionString)
        {
            // Timer that measures how long the entire operation takes
            Stopwatch timer = Stopwatch.StartNew();

            TableServiceClient originTableServClient =
                new TableServiceClient(originConnectionString);
            TableServiceClient destinationTableServClient =
                new TableServiceClient(destinationConnectionString);

            Pageable<TableItem> originTables = originTableServClient.Query();

            int entityCounter = 0;
            Console.WriteLine($"Copying tables from {originTableServClient.AccountName} to "
                + $"{destinationTableServClient.AccountName}.");
            foreach (var ti in originTables)
            {
                System.Console.WriteLine("Table Item name - " + ti.Name);
                if ((ti.Name.Contains("-") || ti.Name.Contains("$")) == false)
                {
                    // for each table that is in the origin create the same table in the destination
                    await destinationTableServClient.CreateTableAsync(ti.Name);

                    // create table clients to interact with origin and destination tables
                    TableClient originTableClient =
                        originTableServClient.GetTableClient(ti.Name);
                    TableClient destinationTableClient =
                        destinationTableServClient.GetTableClient(ti.Name);

                    List<TableEntity> originEntities = 
                        originTableClient.Query<TableEntity>().ToList();

                    if (originEntities.Count < 100)
                    {
                        List<TableTransactionAction> addEntitiesBatch = 
                            new List<TableTransactionAction>();
                        addEntitiesBatch.AddRange(originEntities.Select(oe =>
                            new TableTransactionAction(TableTransactionActionType.Add, oe)));
                        Response<IReadOnlyList<Response>> response = await
                            destinationTableClient.SubmitTransactionAsync(addEntitiesBatch).ConfigureAwait(false);
                    }
                    else if (originEntities.Count > 100)
                    {
                        for (int i = 0; i < originEntities.Count; i = i + 100)
                        {
                            List<TableTransactionAction> addEntitiesBatch = new List<TableTransactionAction>();
                            List<TableEntity> originEntitiesBatch =
                                originEntities.Skip(i).Take(99).ToList();
                            addEntitiesBatch.AddRange(originEntitiesBatch.Select(oe =>
                                new TableTransactionAction(TableTransactionActionType.Add, oe)));
                            Response<IReadOnlyList<Response>> response = await
                                destinationTableClient.SubmitTransactionAsync(addEntitiesBatch).ConfigureAwait(false);
                        }
                    }
                    System.Console.WriteLine($"Copied {originEntities.Count} entities to " +
                        $"{ti.Name}.");
                    entityCounter += originEntities.Count;
                }
            }
            timer.Stop();
            Console.WriteLine($"Transfered {entityCounter} entities in "
                + $"{timer.Elapsed.TotalSeconds} seconds.");
        }

        /// <summary>
        /// Method clears a storage account of all the tables and table entities given the 
        /// connection string to that storage account
        /// </summary>
        /// <param name="connectionString"></param>
        public static async Task DeleteTables(string connectionString)
        {
            // Timer that measures how long the entire operation takes
            Stopwatch timer = Stopwatch.StartNew();
            int tableCount = 0;

            // Delete all tables in the destination 
            TableServiceClient tableServiceClient = new TableServiceClient(connectionString);

            Pageable<TableItem> destinationTables = tableServiceClient.Query();
            System.Console.WriteLine($"Deleting tables from {tableServiceClient.AccountName}");
            foreach (TableItem ti in destinationTables)
            {
                await tableServiceClient.DeleteTableAsync(ti.Name);
                tableCount++;
            }
            timer.Stop();
            Console.WriteLine($"Deleted {tableCount} tables in "
                + $"{timer.Elapsed.TotalSeconds} seconds");
        }
    }
}